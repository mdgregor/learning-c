//
// Created by ferros on 7/31/18.
//

#include <stdio.h>
#include <stdlib.h>

#ifndef CHALLENGELIB_H_
#define CHALLENGELIB_H_

//Global variables
float accountBalance, amount;

//prototypes declarations
void initializeAccount();
void getBalance(void);
void askCustomer(void);
void updateAccount(float value);
void addGift(float giftAmount);
void thankYou(void);

#endif //CHALLENGELIB_H_

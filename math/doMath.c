//
// Created by ferros on 6/24/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void){

    float centimetersInInch = 2.54;
    int inchesInFoot = 12;
    float centimetersInFoot = centimetersInInch * inchesInFoot;
    int value;

    printf("Please enter a number of centimeters: ");
    scanf("%d", &value);

    int totalFeet = value / centimetersInFoot;

    float remainingInches = (value - (totalFeet * centimetersInFoot)) / centimetersInInch;

    printf("The height is %d feet %.2f inches\n", totalFeet, remainingInches);

}


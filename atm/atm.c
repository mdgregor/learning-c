//
// Created by ferros on 7/30/18.
//


#include <stdio.h>
#include <stdlib.h>

float accountBalance, amount;

void initilizeAccount();
void getBalance(void);
void askCustomer(void);
void updateAccount(float value);
void addGift(float giftAmount);
void thankYou(void);

int main(void){
    int transactionCounter = 0;
    initilizeAccount();
    askCustomer();
    return EXIT_SUCCESS;
}

void initilizeAccount(){
    accountBalance = 0;
}

void getBalance(void){
    printf("Current Balance is: %.2f", accountBalance);
}

void askCustomer(void){
    int doUpdate;
    while(doUpdate != 2) {
        printf("Do you wish to update account? (1=Yes, 2=No)");
        scanf("%d", &doUpdate);

        if (doUpdate == 1)
            float value;
            printf("Enter amount to update:");
            scanf("%2f", &value);
            updateAccount(value);

        if (doUpdate != 1)
            thankYou();
}

void updateAccount(float value){
    accountBalance += value;
}

void thankYou(void){
    printf("Thanks");
}
